#include <iostream>
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
        string nombre = "\0";
        string codigo = "\0";

    public:
        /* constructores */
        Proteina ();
        Proteina (string nombre, string codigo);

        /* métodos get and set */
        string get_nombre();
        string get_codigo();
        void set_nombre(string nombre);
        void set_codigo(string codigo);
};
#endif
