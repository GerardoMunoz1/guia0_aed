/*
 * g++ Persona.cpp -o Persona
 *
 * o ejecutar:
 *
 * make
 */

#include <iostream>
using namespace std;
#include "Proteina.h"

/* constructores */
Proteina::Proteina() {
    string nombre = "\0";
    string codigo = "\0";
}

Proteina::Proteina (string nombre, string codigo) {
    this->nombre = nombre;
    this->codigo = codigo;
}

/* métodos get and set */
string Proteina::get_nombre() {
    return this->nombre;
}

string Proteina::get_codigo() {
    return this->codigo;
}

void Proteina::set_nombre(string nombre) {
    this->nombre = nombre;
}

void Proteina::set_codigo(string codigo) {
    this->codigo = codigo;
}
