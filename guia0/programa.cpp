/*
  * g++ programa.cpp Proteina.cpp -o programa
 */
#include <iostream>
#include <list>
#include "Proteina.h"

void print_datosProteina (Proteina p) {

    cout << "\n = IMPRIMIENDO DATOS =";
    cout << "\nNOMBRE: " << p.get_nombre();
    cout << "\nCÓDIGO: " << p.get_codigo() << endl;
}

void ingresar_datosProteina(Proteina p) {

    list<string> nameProtein;
    string nombre_proteina;
    string codigo_proteina;
    //Proteina proteina1 = Proteina();
    cout << "\nIngresando datos: ";
    cout << "\nNOMBRE: ";
    cin >> nombre_proteina;
    p.set_nombre(nombre_proteina);
    nameProtein.push_back(nombre_proteina);

    cout << "CÓDIGO: ";
    cin >> codigo_proteina;
    p.set_codigo(codigo_proteina);
    nameProtein.push_back(codigo_proteina);

    cout << "\n[OK] DATOS INGRESADOS CON ÉXITO!" << endl;

    //cout << "\nNOMBRE: " << p.get_nombre();
    //cout << "\nCÓDIGO: " << p.get_codigo();

    //nameProtein.size(); // Corroboración de funcionamiento.

}

void menu() {
    cout << "\n\t\t= MENU PRINCIPAL =" << endl;

    cout << "\n1. Ingresar Datos Proteina" << endl;
    cout << "2. Ver Datos Proteina." << endl;
    cout << "3. Salir." << endl;
}

int main () {

    menu();

    Proteina proteina1 = Proteina();

    int opcion;
    cout << "OPCION: ";
    cin >> opcion;
    int salir = 0;
    list<string> nameProtein;

    do {
        if (opcion == 1){
            cout << "Opción 1 ingresada - INGRESAR DATOS DE PROTEINA." << endl;

            list<string> nameProtein;
            string nombre_proteina;
            string codigo_proteina;

            cout << "\nIngresando datos: ";
            cout << "\nNOMBRE: ";
            cin >> nombre_proteina;
            proteina1.set_nombre(nombre_proteina);
            nameProtein.push_back(nombre_proteina);

            cout << "CÓDIGO: ";
            cin >> codigo_proteina;
            proteina1.set_codigo(codigo_proteina);
            nameProtein.push_back(codigo_proteina);

            cout << "\n[OK] DATOS INGRESADOS CON ÉXITO!" << endl;

            //cout << "\nNOMBRE: " << proteina1.get_nombre();
            //cout << "\nCÓDIGO: " << proteina1.get_codigo();

            salir = 1;
        }
        else if (opcion == 2){
            cout << "Opción 2 ingresada - IMPRIMIR DATOS DE PROTEINA." << endl;
            print_datosProteina(proteina1);
            salir = 1;

        }
        else if (opcion == 3){
            cout << "\nOpción 3 ingresada - SALIR." << endl;
            cout << "Hasta pronto!" << endl;
            salir = 1;
            break;
        }
        else {
            cout << "Error, inténtalo nuevamente." << endl;
        }

        menu();
        cout << "OPCION: ";
        cin >> opcion;

    }while (salir != 0);

    return 0;
}
